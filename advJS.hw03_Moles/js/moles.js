document.addEventListener("DOMContentLoaded",  () => {
    const game = new Game(10,10);
    game.appendTo(document.body);
});

class Game {
    _cellArr;
    _currentCell;
    _timerId;
    _winner;

    _container;
    _scoreField;
    _gameField;
    _buttonField;
    _dialog;
    constructor(row, column) {
        this._row = row || 10;
        this._column = column || 10;
        this._score = new Score();
        this._levelArr = ["light", "medium", "hard"];
        this._speedArr = [1500, 1000, 500];
        this._sizeArr = [[7, 7], [10, 10], [15, 15]];
        this._speed = 1500;
        this._createLayout();
        this._gainPointPlayer = this._gainPointPlayer.bind(this);
    }
    _createLayout () {
        const container = document.createElement("div");
        container.className = "root";
        container.id = "root";
        const scoreField = document.createElement("div");
        this._score.appendTo(scoreField);
        const gameField = document.createElement("div");
        gameField.append(this._drawGameField());
        const buttonField = document.createElement("div");
        const startButton = this._createButton("button",  "btn btn-primary", "Start", "click", this._start.bind(this));
        const levelButton = this._createSelect("select", "btn btn-secondary", "change", this._setLevel("speed").bind(this), "Level", "light", "medium", "hard");
        const sizeButton = this._createSelect("select", "btn btn-secondary", "change", this._setLevel("size").bind(this), "Size", `S: ${this._sizeArr[0][0]} x ${this._sizeArr[0][1]}`, `M: ${this._sizeArr[1][0]} x ${this._sizeArr[1][1]}`, `L: ${this._sizeArr[2][0]} x ${this._sizeArr[2][1]}`);
        const resetButton = this._createButton("button", "btn btn-light","reset", "click", this._reset.bind(this));
        buttonField.append(startButton);
        buttonField.append(levelButton);
        buttonField.append(sizeButton);
        buttonField.append(resetButton);

        this._dialog = document.createElement("dialog");
        container.append(gameField);
        container.append(buttonField);
        // container.append(scoreField);

        this._gameField = gameField;
        this._buttonField = buttonField;
        this._scoreField = scoreField;
        this._container = container;
    }
    appendTo(elem) {
        elem.append(this._container);
    }

    _drawGameField () {
        const table = document.createElement("table");

        const cellArr = [];
        for (let i = 0; i < this._row; i++){
            cellArr[i] = [];
            const newRow = document.createElement("tr");
            for (let j = 0; j < this._column; j++) {
              const cell = document.createElement("td");
                cellArr[i][j] = cell;
                // cell.innerText = `${i}${j}`;
              newRow.appendChild(cell);
            }
            table.append(newRow);
        }
        console.log(cellArr);
        this._cellArr = cellArr.flat();
        return table;
    }

    _createButton(elem = "button", className, text, event = 0, func) {
        const newElem = document.createElement(elem);
        newElem.className = className;
        newElem.innerText = text;
        if (event) {
            newElem.addEventListener(event, function (e) {
                func(e)
            });
        }
        return newElem;
    }
    _createSelect(elem = "select", className, event, func, value, value1, value2, value3) {
        const newElem = document.createElement(elem);
        newElem.className = className;
        newElem.innerHTML = `<option value="none" hidden="">Select ${value}</option> 
  <option value="light">${value1}</option>
  <option value="medium">${value2}</option>
  <option value="hard">${value3}</option>`
        if (event) {
            newElem.addEventListener(event, function (e) {
                func(e)
            });
        }
        return newElem;
    }

    _randomize(max) {
        return Math.floor(Math.random()*max)
    }
    _getRandomCell () {
        const max = this._cellArr.length;
        console.log(`max/length = ${max}`);
        const index = this._randomize(max);
        console.log(index);
        const randomCell = this._cellArr[index];
        this._cellArr.splice(index, 1);
        return randomCell;
    }

    _setLevel(type) {
        return function (event) {
            const level = event.target.value;
            console.log(type, level);
            const index = this._levelArr.findIndex(item => item === level);
            switch (type) {
                case "speed":
                    this._speed = this._speedArr[index];
                    console.log(this._speed);
                    break;
                case "size":
                    ([this._row, this._column] = this._sizeArr[index]);
                    console.log(this._row, this._column);
                    this._cleanBoard();
                    this._createLayout();
                    this.appendTo(document.body);
                    break;
            }
        }
    }

    _cleanBoard() {
        document.getElementById("root").remove();
    }

    _start (event) {
        this._container.append(this._scoreField);
        document.querySelectorAll("select").forEach(item => item.setAttribute("disabled", true)) ;
        event.target.setAttribute("disabled", true);
        this._runNextRound();
    }

    _reset(){
        clearTimeout(this._timerId);
        this._cleanBoard();
        this._score = new Score();
        this._speed = 1500;
        this._createLayout();
        this.appendTo(document.body)
    }

    _checkWinner() {
        const index = this._score._score.findIndex((elem) => elem >= this._row * this._column / 2);
        if (index === -1) {
            return true
        } else if (index) {
            this._winner = index;
            return false
        } else {
            this._winner = index;
            return false
        }
    }

    _showWinner() {
        const message = this._winner ? "You are Looser! &#9785" : "Congratulations!<br>Looks like You won this one &#9787";

            if (typeof this._dialog.showModal === "function") {
                const html = `<form method="dialog"><p>${message}</p><menu><button id="confirmBtn">Try again &#8634</button></menu></form>`;
                this._dialog.insertAdjacentHTML("afterbegin", html);
                this._container.append(this._dialog);
                this._dialog.showModal();
                const confirmBtn = document.getElementById("confirmBtn");
                confirmBtn.addEventListener("click", this._reset.bind(this));
            }
            else {
                alert(message);
                this._reset();
            }
    }

    _runNextRound() {
        if (this._checkWinner()) {
            this._currentCell = this._getRandomCell();
            this._currentCell.className = "picked";
            console.log(this._currentCell);
            this._currentCell.addEventListener("click", this._gainPointPlayer);
            this._timerId = setTimeout(this._gainPointComp.bind (this), this._speed)
        } else this._showWinner();
    }

    _gainPointComp() {
        this._currentCell.className = "pointToComp";
        this._currentCell.removeEventListener("click", this._gainPointPlayer);
        this._score.increase("comp");
        this._runNextRound();
    }
    _gainPointPlayer() {
        clearTimeout(this._timerId);
        this._currentCell.className = "pointToPlayer";
        console.log('currentCell',this._currentCell);
        this._currentCell.removeEventListener("click", this._gainPointPlayer);
        this._score.increase("player");
        this._runNextRound();
    }
}

class Score {
    constructor() {
        this._scoreBoard = null;
        this._playerScore = 0;
        this._compScore = 0;
        this._createLayout();
    }

    _createLayout(){
        this._scoreBoard = document.createElement("span");
        this._updateLayout();
    }
    _updateLayout() {
        this._scoreBoard.innerText = `Score => Player: ${this._playerScore} vs. Comp: ${this._compScore}`;
    }
    appendTo(element) {
        element.append(this._scoreBoard);
    }

    increase(id) {
        switch (id) {
            case "player":
                this._playerScore++;
                this._updateLayout();
                break;
            case "comp":
                this._compScore++;
                this._updateLayout();
                break;
            default:
                console.log("score motherfucker");
        }
    }

    get _score () {
        return [this._playerScore, this._compScore];
    }
}




