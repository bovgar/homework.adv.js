const movies = request ({
    type: "GET",
    url: "https://swapi.co/api/films/",
    func: render,
    successID: "movies",
    progressID: "progress",
    loadFunc: load,
});

function request ({type, url, func, successID, loadFunc, progressID}) {
    const request = new XMLHttpRequest();
    request.open(type, url);
    request.responseType = "json";
    request.send();
    try {
        request.onload = function () {
            if (request.status < 300) {
                console.log(request.response);
                func(request.response, successID)
            } else {
                throw new Error("something went wrong")
            }
        };
    }
    catch {
        e.message
    }
    request.onerror = function (event, errorID) {
        console.log("You are looser");
        document.getElementById(errorID).innerText = "You are looser";
    };
    request.onprogress = function (event) {
       const data = `Loaded ${event.loaded} bit`;
        console.log(`Loaded ${event.loaded} bit`);
        loadFunc (data, progressID);
    }
}

function render (response, id) {
    const container = document.getElementById(id);
    const content = response.results;
    if (typeof content === "string") {
        container.innerText = content;
    } else if (Array.isArray(content)) {
        console.log(Array.isArray(content));
        let moviesHtml ="";
        for (let episode = 1; episode <= content.length; episode++) {
            const foundEpisode = content.find(element => element.episode_id === episode);
            const id = foundEpisode.episode_id;
            const title = foundEpisode.title;
            const characters = foundEpisode.characters;
            const crawl = foundEpisode.opening_crawl;
            moviesHtml += `<li><p class="font-weight-normal">Episode # ${id}</p>
                                <p class="font-weight-bold">${title}</p>
                                <ol id="characters-episode${episode}" class="font-weight-normal">Characters:</ol>
                                <p class="font-italic">${crawl}</p></li>`;
            characters.forEach(character => request({
                type: "GET",
                url: `${character}`,
                func: renderCharacters,
                successID: `characters-episode${episode}`,
                loadFunc: load,
                progressID: "progress",
            }));
        }
        container.innerHTML = moviesHtml;
    }
}

function renderCharacters(response, id) {
    // console.log(response);
    const container = document.getElementById(id);
    console.log(container);
    const elem = document.createElement("li");
    elem.innerText = response.name;
    container.appendChild(elem);
}

function load (data, id) {
    const container = document.getElementById(id);
    container.innerText = data;
}